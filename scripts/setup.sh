#!/usr/bin/env bash
####################################################################################################
# Makes sure the project is completely setup:
#	- it will pull from git any project you don't have in the project root
#	- it will will build them. Only those that were not present
####################################################################################################
# set -x

# setup defaults
POSITIONAL=()

###########################################################################
# functional support
###########################################################################
function _usage {
	echo "Description: makes sure the project is completely setup"
	echo "Usage: ./scripts/setup.sh [-h|--help]"
	echo "   -h|--help: shows usage information"
}

function _setupProject {
	PROJECT="${1}"
	if [[ ! -e "${PROJECT}" ]]
	then
		REPOSITORY=${2}
		echo "${PROJECT}: cloning and building"
		# clone the project into target directory
		git clone "${REPOSITORY}" "./${PROJECT}"
		# get in there and install his dependencies
		pushd ${PROJECT} > /dev/null
		npm install
		echo ""
		popd > /dev/null
	fi
}

###########################################################################
# action
###########################################################################
# make sure we are where we think we are
if [[ -e "./setup.sh" ]]
then
	cd ..
elif [[ ! -e "./scripts/setup.sh" ]]
then
	echo "Error: must be run from project root"
	_usage
	exit 1
fi

# parse the command line
while [[ ${#} -gt 0 ]]
do
	case "$1" in
		-h|--help)
			_usage
			exit 1
			;;
		-*)	
			echo "Error: unknown option \"${OPTION}\""
			_usage
			exit 1
			;;
		*)
			POSITIONAL+=("$1")
			shift;
			;;
	esac
done

_setupProject server "https://fishguts@bitbucket.org/fishguts/pig-dam-server.git"
_setupProject factory "https://fishguts@bitbucket.org/fishguts/pig-dam-factory.git"
_setupProject mongo "https://fishguts@bitbucket.org/fishguts/pig-dam-mongo.git"
_setupProject search "https://fishguts@bitbucket.org/fishguts/pig-dam-search.git"
_setupProject settings "https://fishguts@bitbucket.org/fishguts/pig-dam-settings.git"
