#!/usr/bin/env bash
####################################################################################################
# capable of the following tasks:
#	- start: setup and start development environment. Builds and starts containers for all projects except for those in params
#	- stop: stop development environment. Kills all containers for all projects except for those in params
####################################################################################################
# set -x

# setup defaults
export NODE_ENV=test
ACTION=""
EXCLUDES=""
# note: the order can matter. Start those that don't have dependencies first:
PROJECTS="settings mongo search factory server"

###########################################################################
# functional support
###########################################################################
function _usage {
	echo "Description: starts and stops environments"
	echo "Usage: ./scripts/suite.sh [-h|--help] [-e|--nodenv local|test|staging|production] [-x|--exclude project [...project]] <action:start|stop>"
	echo "   -e|--nodenv: set the environment NODE_ENV value. Default value=${NODE_ENV}"
	echo "   -h|--help: shows usage information"
	echo "   -x|--exclude: projects on which the action should not happen"
	echo "   start: \"docker:starts\" containers for all projects except for those excluded by -x|--exclude"
	echo "   stop: \"docker:clean\" all containers for all projects except for those excluded by -x|--exclude"
}


function _isRunning  {
	PROJECT="${1}"
	CONTAINER="pig.dam.${PROJECT}"
	docker ps --filter="name=${CONTAINER}" --filter="status=running" | grep "${CONTAINER}" > /dev/null
	RESULT=$?
	# here we look for containers that are not started but as "exited", "paused", etc.. When they exist they
	# prevent us from creating a new container in it's place.
	if [[ ${RESULT} -ne 0 ]]
	then
		docker rm -f "${CONTAINER}" > /dev/null
	fi
	return ${RESULT}
}

function _startDocker {
	PROJECT="${1}"
	if [[ -e "${PROJECT}" ]]
	then
		echo "${PROJECT}: building and running docker container"
		# get in there and start his engines
		pushd ${PROJECT} > /dev/null
		npm run "docker:start"
		echo ""
		popd > /dev/null
	fi
}

function _stopDocker {
	PROJECT="${1}"
	if [[ -e "${PROJECT}" ]]
	then
		echo "${PROJECT}: stopping and removing docker container"
		# get in there and stop his engines
		pushd ${PROJECT} > /dev/null
		npm run "docker:clean"
		echo ""
		popd > /dev/null
	fi
}

###########################################################################
# action
###########################################################################
# make sure we are where we think we are
if [[ -e "./suite.sh" ]]
then
	cd ..
elif [[ ! -e "./scripts/suite.sh" ]]
then
	echo "Error: must be run from project root"
	_usage
	exit 1
fi

# parse the command line
while [[ ${#} -gt 0 ]]
do
	case "${1}" in
		-h|--help)
			_usage
			exit 1
			;;
		-e|--nodenv)
			shift
			NODE_ENV="${1}"
			shift
			;;
		-x|--exclude)
			shift
			EXCLUDES+="${1} "
			shift
			;;
		-*)	
			echo "Error: unknown option \"${OPTION}\""
			_usage
			exit 1
			;;
		*)
			ACTION="${1}"
			shift;
			;;
	esac
done

# Find the projects we want to act upon. It's a negation effect - those specified get spared
if [[ -n "${EXCLUDES}" ]]
then
	for EXCLUDE in ${EXCLUDES}
	do
		PROJECTS=`echo "${PROJECTS}" | sed "s/${EXCLUDE}//g"`
	done
fi

if [[ "${ACTION}" == "start" ]]
then
	# first lets make sure our network bridge is setup
	npm run docker:network:start
	# now start our project engines
	for PROJECT in ${PROJECTS}
	do
		_isRunning "${PROJECT}"
		if [[ $? -eq 0 ]]
		then 
			echo "${PROJECT}: already started"
		else
			_startDocker "${PROJECT}"
		fi
	done
elif [[ "${ACTION}" == "stop" ]]
then
	for PROJECT in ${PROJECTS}
	do
		_stopDocker "${PROJECT}"
	done
elif [[ -z "${ACTION}" ]]
then
	echo "Error: must specify an action"
	_usage
	exit 1
else
	echo "Error: unknown action \"${ACTION}\""
	_usage
	exit 1
fi
