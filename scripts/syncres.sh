#!/usr/bin/env bash
####################################################################################################
# synchronizes all configuration resources from "settings" to the rest of the projects in the suite
####################################################################################################
# set -x

PROJECTS="factory mongo search server settings"
ENVIRONMENTS="local test"

function _usage {
	echo "Description: generates settings for 1 or more projects and environments"
	echo "_usage: $0 [-h|--help] [-e|--envs  env-list] [-p|--projects project-list]"
	echo "   -h|--help: shows _usage information"
	echo "   -e|--envs: environments for which to generate settings. Defaults to \"${ENVIRONMENTS}\""
	echo "   -p|--projects: projects for which to generate settings. Defaults to \"${PROJECTS}\""
}

# if we are in the scripts directory then go to parent project
if [[ -e "./syncres.sh" ]]
then
	cd ..
fi

if [[ ! -d "./settings" ]]
then
	echo "Error: cannot find ./settings"
	exit 1
fi

cd "./settings"

while [[ ${#} -gt 0 ]]
do
	case "${1}" in
		-h|--help)
			_usage
			exit 1
			;;
		-e|--envs)
			shift
			ENVIRONMENTS="${1}"
			shift
			;;
		-p|--projects)
			shift
			PROJECTS="${1}"
			shift
			;;
		-)
			echo "Error: unknown option \"${OPTION}\""
			__usage
			exit 1
			;;
		*)
			break
			;;
	esac
done

for PROJECT in ${PROJECTS}
do
	cp "./res/configuration/readme.md" "../${PROJECT}/res/configuration"
	for ENVIRONMENT in ${ENVIRONMENTS}
	do
		PROJECT_DIRECTORY="../${PROJECT}/res/configuration/${ENVIRONMENT}"
		echo "generating \"${PROJECT}/res/configuration/${ENVIRONMENT}"\"
		mkdir -p "${PROJECT_DIRECTORY}"
		# when we stream directly to stdout we have problems when generating the settings module's own
		# settings configurations.  By the time he goes to read them the pipe has already wiped the PROJECT clean
		RESULT=$(./src/pigcmd.js gencfg -e "${ENVIRONMENT}" "${PROJECT}")
		if [[ ${?} == 0 ]]
		then
			echo "${RESULT}" > "${PROJECT_DIRECTORY}/settings.json"
		else
			exit 1
		fi
	done
done
