#!/usr/bin/env bash
################################################################################################
# run git command on all of our little buddies
################################################################################################

if [[ $# < 1 ]]
then
	echo "Usage: $0 <action> [params]"
	exit 1
elif [[ -e ./run.sh ]]
then
	run.sh git "${@}"
else
	./scripts/run.sh git "${@}"
fi
