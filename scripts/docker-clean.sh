#!/usr/bin/env bash
################################################################################################
# removes docker containers and images
################################################################################################
# set -x

function _usage {
	echo "Description: removes containers and images by name if they exist"
	echo "Usage: $0 [-h|--help] <container> [container...]"
	echo "   -h|--help: shows usage information"
}

for OPTION in "${@}"
do
	case "${OPTION}" in
		-h|--help)
			_usage
			exit 1
			;;
		-*)	
			echo "Error: unknown option \"${OPTION}\""
			_usage
			exit 1
			;;
		*)
			break
			;;
	esac
done

if [[ $# < 1 ]]
then
	_usage
	exit 1
else
	for NAME in "${@}"
	do
		CONTAINERS=$(docker ps -a -q -f ancestor="${NAME}")
		if [[ -n "${CONTAINERS}" ]]
		then
			docker rm -f "${CONTAINERS}"
			docker rmi -f "${NAME}"
		fi
	done
fi
