#!/usr/bin/env bash
####################################################################################################
# synchronizes all code that should be in a lib from one source to one or more targets
####################################################################################################
# set -x

if [[ $# < 1 ]]
then
	printf "usage: $0 <source> [target]\n"
	printf "   source: factory|mongo|search|server|settings\n"
	printf "   target: factory|mongo|search|server|settings\n\n"
	printf "Copies <source>/src/[select-files] to each target: <targets>/src/[select-files]\n"
	exit 1
fi

# if we are in the scripts directory then go to parent project
if [[ -e "./syncsrc.sh" ]]
then
	cd ..
fi

if [[ ! -e "./$1" ]]
then
	echo "Error: cannot find $1"
	exit 1
fi

# the source project from which we will pull source code
SOURCE=$1
# find our targets
if [[ $# > 1 ]]
then
	TARGETS=$2
else
	# filter the source project out of the targets
	TARGETS=`echo "factory mongo search server settings" | sed "s/${SOURCE}//"`
fi

function copyCoreFiles {
	# src/root
	copyFile "src/pigcmd.js"
	# src/command
	copyFile "src/command/setup.js"; copyFile "test/unit/command/test-setup.js"
	# src/common
	copyFile "src/common/http.js"; copyFile "test/unit/common/test-http.js"
	copyFile "src/common/io.js"; copyFile "test/unit/common/test-io.js"
	copyFile "src/common/job.js"; copyFile "test/unit/common/test-job.js"
	copyFile "src/common/setup.js"; copyFile "test/unit/common/test-setup.js"
}

function copyServerFiles {
	# src/route
	copyFile "src/route/endpoint/diagnostics.js"; copyFile "test/unit/route/endpoint/test-diagnostics.js"
	# test/support
	copyFile "test/support/setup.js"
	copyFile "test/support/command/spawn.js"
	# test/integration
	copyFile "test/integration/workflow/test-ping.js"
}

for TARGET in ${TARGETS}
do
	function copyFile {
		mkdir -p `dirname "${TARGET}/${1}"`
		cp -R "${SOURCE}/${1}" "${TARGET}/${1}"
	}

	echo "copying \"${SOURCE}/src/...\" to \"${TARGET}/src/..."\"
	# everybody gets the core files
	copyCoreFiles
	# copy express support to our suite of servers.
	if [[ "${TARGET}" == "server" || "${TARGET}" == "factory" || "${TARGET}" == "settings" ]]
	then
		copyServerFiles
	fi
done
