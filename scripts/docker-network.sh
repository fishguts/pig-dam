#!/usr/bin/env bash
################################################################################################
# manages our docker network
################################################################################################
# set -x

NETWORK_NAME="pig.dam.network"
NETWORK_RUNNING=$(docker network ls --filter "name=${NETWORK_NAME}" --format {{.Name}})
OPERATION="$1"

function _usage {
	echo "Description: starts and stops the \"pig.dam.network\" docker network"
	echo "Usage: $0 <start|stop>"
	echo "   -h|--help: shows usage information"
}

for OPTION in "${@}"
do
	case "${OPTION}" in
		-h|--help)
			_usage
			exit 1
			;;
		-*)	
			echo "Error: unknown option \"${OPTION}\""
			_usage
			exit 1
			;;
		*)
			break
			;;
	esac
done

if [[ "${OPERATION}" == "start" ]]
then
	if [[ -z "${NETWORK_RUNNING}" ]]
	then
		docker network create "${NETWORK_NAME}"
	fi
elif [[	"${OPERATION}" == "stop" ]]
then
	if [[ -n "${NETWORK_RUNNING}" ]]
	then
		docker network rm "${NETWORK_NAME}"
	fi
else
	_usage
	exit 1
fi
