# Pig Digital Asset Manager

## Overview

Pig is a foundation for asset management.  It encapsulates:

- File System Emulation: we say emulation because Pig's API uses the filesystem native to the OS for storage. Pig exist on top of the native filesystem layer.  The filesystem native to the OS is completely abstracted from clients of the API.  
- Versioning
- Branching
- Searching:
	- by user metadata
	- by exif metadata
	- by file contents
- Database (in the concept phase)

## Architecture

The project utilizes a micro-service architecture. It is broken up into:

- [dam](./readme.md) the project container with support for:
	- setting up the project
	- building the project
	- updating the project
- [factory](./factory/readme.md) the brains behind asset properties
- [search](./search/readme.md) facilitates indexing of our assets and it's metadata
- [server](./server/readme.md) which is the content management administrator.
- [settings](./settings/readme.md) manages and serves an environments configuration

## Build

The build exists at a *macro* and *micro* level.  The macro level is responsible for setting up *pig*, building *pig*, running *pig*, tearing down *pig*, etc.  The micro level is the build scripts and CLI that exist within each micro-service project. We will only document and deal with the macro level here (which utilizes the builds within each micro-service).

There are really only two areas of interest:
 
- [package.json](./package.json) has scripts for the following:
	- `docker:build` configures environment and runs build on each micro-service.
	- `docker:clean` shuts the machine down and removes all ontainers.
	- `docker:start` builds and starts each container.
	- `test` runs unit and integration tests on each micro-service.
	- `test:ping` spawns a server for each micro-service and pings it to make sure it is healthy.
	- and a handful of guys we use to support the above.
- [scripts](./scripts) which has scripts for admining the project:
	- [git.sh](./scripts/git.sh) runs the specified git arguments on each micro-service. 
	- [npm.sh](./scripts/npm.sh) runs the specified npm arguments on each micro-service. 
	- [setup.sh](./scripts/setup.sh) he's a little bit of a swiss army knife:
		- `./scripts/setup.sh env` will pull down and setup all micro-service dependencies/projects that are not already pulled down. 
		- `./scripts/setup.sh [options] start [exclude]` will build and run all projects except the optional exclusion params which is handy for isolating a service that you may want to run in debug mode.
		- `./scripts/setup.sh stop` effectively the same as `npm docker:clean`

## Getting Started

1. move to a directory you want to contain the parent directory.
2. clone pig: `git clone https://fishguts@bitbucket.org/fishguts/pig-dam.git <target>`
3. enter pig: `cd <target>`
4. get his mates: `./scripts/setup.sh env`
5. startup the machine: `npm run docker:start` _or_ `./scripts/setup.sh start` 
6. pig should be up and running on your machine: `docker ps`. You should see: *pig.dam.server*, *pig.dam.factory*, *pig.dam.mongo*, *pig.dam.search* and *pig.dam.settings*
7. You may explore the deployment with pre-baked *postman* services: Import [./test/curt-pig.postman_collection.json](./test/curt-pig.postman_collection.json) into [postman](https://www.getpostman.com/)
	* Note: attachments do not persist in _postman_. For the _content create_ and _content update_ service you will need to attach (and re-attach every time you launch _postman_) your own test content. See the request body and you will see _content_ and a _Choose Files_.  Choose any content file you like for testing. There are some content files at your disposal in [./server/test/data](./server/test/data)



